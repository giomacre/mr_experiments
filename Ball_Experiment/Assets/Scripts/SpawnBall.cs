using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBall : MonoBehaviour
{
    public GameObject spherePrefab;

    public void SpawnPrefab()
    {
        var objectPosition = transform.position + new Vector3(0, 0.1f, 0);
        _ = Instantiate(spherePrefab, objectPosition, Quaternion.identity);
    }
}
