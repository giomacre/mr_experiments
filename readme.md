# Riferimenti Principali

- [Documentazione Unity](https://docs.unity3d.com/Manual/)
- [Documentazione MRTK](https://docs.microsoft.com/en-us/windows/mixed-reality/mrtk-unity/)
- [Fellow Oak DICOM Repo](https://github.com/fo-dicom/fo-dicom)
- [Unity MRTK Repo](https://github.com/microsoft/MixedRealityToolkit-Unity)
- [Introduzione a DICOM](https://saravanansubramanian.com/dicomintro)
- [DICOM Standard Browser](https://dicom.innolitics.com/ciods)
- [Rappresentazioni di rotazioni](https://stevendumble.com/attitude-representations-understanding-direct-cosine-matrices-euler-angles-and-quaternions/)
- [Repo progetto glTF](https://github.com/KhronosGroup/glTF)
- [Repo FBX2glTF per la conversione di modelli](https://github.com/facebookincubator/FBX2glTF)


# Principali asset inclusi nel progetto

-  `Meshes/`
    - **kidney.fbx** Il modello dei reni fornito nel progetto di partenza.
    - **Kidneys.obj** Una versione del modello dei reni ottenuta con 3DSlicer, usato per comprendere meglio il legame tra il sistema di riferimento dei modelli importati in Unity e il sistema di riferimento clinico usato nella definizione dello standard DICOM. 
    - **Skull.obj** Un modello 3D di un cranio ottenuto usando 3DSlicer. 


- `Plugins/` Contiene il codice sorgente del progetto FO-DICOM.Core con le sue dipendenze compilate per la versione 2.0 del .NET Standard che è la versione supportata dalla versione 2020 di Unity.
    Gli assembly delle dipendenze sono stati presi dai corrispondenti pacchetti nuget.


- `Streaming Assets/`
    - `DICOM/` 
          - `kidneys/` Dataset di immagini in piano frontale dell'addome di un paziente.
          - `skull/` CT scan in piano traverso di una testa, nell'ultimo commit il numero di immagini è stato dimezzato per velocizzarne il caricamento, quindi per consultare il dataset originale è necessario fare il checkout del commit originale.
    - `Models/` 
           - **kidney.glb** Versione in formato `glTF` del modello dei reni originale, per il caricamento a runtime.

- `Shaders/`
    Contiene una versione modificata dello `StandardShader` del MRTK per consentire il clipping in contemporanea da tre piani diversi. 

- `Materials/`
    - **ClippingPlane.mat** Materiale per la visualizzazione del piano di clipping.
    - **DoubleFacedRenderer** Utilizza lo `StandardShader` fornito in MRTK configurato per disabilitare il culling dei vertici e la definizione a compile-time dell'albedo, in modo da potere essere utilizzato su un elemento `Quad` di Unity per visualizzare texture caricate a runtime.
    - **ModelMaterial.mat** Usa lo shader customizzato per abilitare il clipping del modello.
- `Scripts/`
    - `MeshLoader/` Componente necessaria al caricamento a runtime dei modelli, include una classe template con tre implementazioni per il caricamento tramite File, HTTP, o, in caso di modelli supportati dalla pipeline di Unity, normale caricamento di asset serializzati. 
    - `UI/`
        * **SliceSlider.cs** Implementazione di [IMixedRealityPointerHandler](https://docs.microsoft.com/en-us/dotnet/api/microsoft.mixedreality.toolkit.input.imixedrealitypointerhandler) per l'interazione con i piani di interazione delle immagini.
        Consente lo spostamento del piano lungo la sua direzione normale rispettando le posizioni indicate nei file DICOM.
        Inoltre usa gli eventi di Unity per permettere ad altri componenti di osservare il cambiamento di posizione e l'inizio e la fine di ogni interazione.
    - **DicomFileUtils.cs** Contiene le funzioni per il caricamento dei file DICOM da directory. 
    - **GeometryExtensions.cs** Estensioni per la classe `FrameGeometry` di fo-dicom in particolare include funzioni per:
        * Ricavare un vettore di scala per il piano di visualizzazione dall'attributo [Pixel Spacing](https://dicom.innolitics.com/ciods/rt-dose/image-plane/00280030) dei file DICOM.
        * Convertire la rotazione dell'immagine rispetto al sistema di riferimento del paziente, dai [vettori unitari](https://dicom.innolitics.com/ciods/rt-dose/image-plane/00200037) presenti nei file DICOM, alla rappresentazione secondo gli angoli di Eulero per l'ordinamento ZXY usato da Unity.
        * Proiezione del vettore fornito dall'attributo [Image Position](https://dicom.innolitics.com/ciods/rt-dose/image-plane/00200032) sul sistema di riferimento del paziente, per il posizionamento corretto della superficie di visualizzazione.
    - **DicomViewer.cs** Lo script per il componente principale della scena, dopo avere caricato i file DICOM e la mesh del modello procede ad inizializzare le proprietà dei piani di visualizzazione corrispondenti ai punti di vista usati nel dataset caricato, e l'orientamento del modello.
- `Scenes/`
    - **Main.unity** La scena principale del progetto contiene tre esempi di DicomViewer, uno per il dataset del cranio, uno per quello dei reni, e uno che utilizza una versione modificata dello script per abilitare tutti i piani di visualizzazione per testare il funzionamento dello shader modificato.

# Motivazione per l'inclusione del codice di FO-DICOM.Core
Con le versioni di Unity a partire dalla 2019,
l'unico backend per la compilazione degli script è IL2CPP, per l'ottimizzazione dei progetti tramite la compilazione AOT. 

Quando il progetto viene lanciato nell'Editor di Unity la libreria funziona correttamente perchè viene utilizzata la normale compilazione JIT, mentre quando viene inclusa un assembly precompilato della libreria fo-dicom il processo di traduzione da Intermidiate Language a C++ sembra non funzionare correttamente in quanto una volta che il progetto viene compilato e installato sul device, il [codice](https://github.com/fo-dicom/fo-dicom/tree/development/FO-DICOM.Core/IO/Buffer) relativo all'IO fallisce e lancia delle eccezioni, questo è stato verificato tramite il debug da remoto spiegato in questo [articolo](https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/managed-debugging-with-unity-il2cpp) della documentazione di MRTK.

Il codice sorgente è incluso in `fodicom_experiments/Assets/Plugins/FO-DICOM`,
alcuni degli asset che normalmente verrebbero inclusi nella libreria compilata sono inclusi per il caricamento a runtime in `fodicom_experiments/Assets/StreamingAssets/FO-DICOM`.

Nel file `fodicom.patch` sono presenti le modifiche necessarie al utilizzo in Unity, incluso il [codice](https://github.com/fo-dicom/fo-dicom/issues/888) del vecchio plugin presente sullo store di Unity per il rendering di immagini in Texture di Unity.
Un'implementazione alternativa del modulo di rendering potrebbe essere il progetto attualmente in sviluppo presente in [questa](https://github.com/MichaelOvens/fo-dicom-unity) repo.

# Problemi Irrisolti

## Perfezionamento di corrispondenza tra immagini e Modelli
Nella versione attuale del progetto l'allineamento dell'immagine viene effetuato centrando la bounding box del modello sull'asse di scorrimento del piano dell'immagine. Il metodo ottiene risultati abbastanza buoni ma per avere una corrispondenza perfetta andrebbe usati metodi più sofisticati.

## Ottimizzazione dei modelli
I modelli ottenuti da file DICOM hanno un numero molto elevato di vertici, il che limita fortemente le performance su Hololens a causa dell'hardware limitato del dispositivo.

## Caricamento di modelli con submesh
La componente [ClippingPrimitive](https://docs.microsoft.com/en-us/dotnet/api/microsoft.mixedreality.toolkit.utilities.clippingprimitive) di MRTK definisce dei metodi per l'aggiunta dinamica di renderer alla lista di oggetti da tagliare, tuttavia nelle prove effettuate questi non sembravano funzionare correttamente.

Per aggirare questo problema ogni piano di visualizzazione è collegato manualmente ad un singolo oggetto che viene incluso nella scena. Questa soluzione non rende possibile visualizzare modelli con submesh come l'asset `Kidneys.obj`.

## Caricamento di File Dicom da HTTP
Nella versione corrente i modelli possono essere caricati tramite una richiesta GET, a patto che siano in formato `.glb` (glTF binario). Mentre il caricamento di file DICOM rimane limitato alla lettura di `Streaming Assets/`, questo perchè dalle prove svolte con file `.zip` è risultato che la componente `DicomFile` di fo-dicom non supporti la lettura da `DeflateStream` di `System.IO.Compression`, inoltre Unity non include l'assembly di `System.IO.Compression.FileSystem` il che limita le operazioni possibili sugli archivi `.zip`.
